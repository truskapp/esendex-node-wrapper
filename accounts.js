var xmlparser_1 = require('./xmlparser');
var util_1 = require('util');
var Accounts = (function () {
    function Accounts(client) {
        this.client = client;
        this.parser = new xmlparser_1.XmlParser();
    }
    Accounts.prototype.get = function (options, callback) {
        var _this = this;
        var path = '/v1.0/accounts';
        var accountId = options && options.id || null;
        if (accountId) {
            path = util_1.format('%s/%s', path, accountId);
            delete options.id;
        }
        this.client.requesthandler.request('GET', path, options, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                if (accountId)
                    return callback(null, result.account);
                var accounts = result.accounts;
                if (!util_1.isArray(accounts.account))
                    accounts.account = [accounts.account];
                callback(null, accounts);
            });
        });
    };
    return Accounts;
})();
exports.Accounts = Accounts;
