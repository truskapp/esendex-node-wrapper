var xmlparser = require('./xmlparser');
var xmlbuilder = require('./xmlbuilder');
var util = require('util');
var Messages = (function () {
    function Messages(client) {
        this.client = client;
        this.builder = new xmlbuilder.XmlBuilder('messages');
        this.parser = new xmlparser.XmlParser();
    }
    Messages.prototype.get = function (options, callback) {
        var _this = this;
        var path = '/v1.0/messageheaders';
        var messageId = options && options.id || null;
        if (messageId) {
            path = util.format('%s/%s', path, messageId);
            delete options.id;
        }
        this.client.requesthandler.request('GET', path, options, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                if (messageId)
                    return callback(null, result.messageheader);
                var messageheaders = result.messageheaders;
                if (!util.isArray(messageheaders.messageheader))
                    messageheaders.messageheader = [messageheaders.messageheader];
                callback(null, messageheaders);
            });
        });
    };
    Messages.prototype.send = function (messages, callback) {
        var _this = this;
        var xml = this.builder.build(messages);
        this.client.requesthandler.request('POST', '/v1.0/messagedispatcher', null, xml, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                var messageheaders = result.messageheaders;
                if (!util.isArray(messageheaders.messageheader))
                    messageheaders.messageheader = [messageheaders.messageheader];
                callback(err, messageheaders);
            });
        });
    };
    Messages.prototype.getBody = function (messageId, callback) {
        var _this = this;
        var path = util.format('/v1.0/messageheaders/%s/body', messageId);
        this.client.requesthandler.request('GET', path, null, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                callback(err, result.messagebody);
            });
        });
    };
    return Messages;
})();
exports.Messages = Messages;
