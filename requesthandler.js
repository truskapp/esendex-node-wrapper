var http = require('http');
var https = require('https');
var querystring = require('querystring');
var responsehandler = require('./responsehandler');
var useragentbuilder = require('./useragentbuilder');
var defaultOptions = {
    host: 'api.esendex.com',
    port: 443,
    https: true,
    timeout: 30000
};
function onSocketTimeout() {
    this._req.abort();
}
var RequestHandler = (function () {
    function RequestHandler(options) {
        this.options = {
            host: options && options.host !== undefined ? options.host : defaultOptions.host,
            port: options && options.port !== undefined ? options.port : defaultOptions.port,
            https: options && options.https !== undefined ? options.https : defaultOptions.https,
            timeout: options && options.timeout !== undefined ? options.timeout : defaultOptions.timeout,
            username: options && options.username !== undefined ? options.username : defaultOptions.username,
            password: options && options.password !== undefined ? options.password : defaultOptions.password
        };
        this.responseHandler = new responsehandler.ResponseHandler();
        this.userAgent = useragentbuilder.UserAgentBuilder.Build();
    }
    RequestHandler.prototype.request = function (method, path, query, data, expectedStatus, callback) {
        var _this = this;
        var isPost = /(POST|PUT)/.test(method);
        var headers = {
            'Accept': 'application/xml',
            'User-Agent': this.userAgent
        };
        var options = {
            host: this.options.host,
            port: this.options.port,
            path: path + (query ? '?' + querystring.stringify(query) : ''),
            method: method,
            headers: headers,
            auth: this.options.username + ':' + this.options.password
        };
        var dataBuf;
        if (isPost) {
            dataBuf = data ? new Buffer(data, 'utf8') : null;
            headers['Content-Type'] = 'application/xml';
            headers['Content-Length'] = data ? dataBuf.length : 0;
        }
        var proto = this.options.https ? https : http;
        var req = proto.request(options, function (res) {
            _this.responseHandler.handle(res, expectedStatus, callback);
        });
        req.on('socket', function (socket) {
            socket._req = req;
            if (_this.options.timeout) {
                socket.setTimeout(_this.options.timeout);
                if (socket.listeners('timeout').length === 0) {
                    socket.on('timeout', onSocketTimeout);
                }
            }
        });
        req.on('error', function (err) {
            callback(err);
        });
        if (isPost && data)
            req.write(dataBuf);
        req.end();
    };
    return RequestHandler;
})();
exports.RequestHandler = RequestHandler;
