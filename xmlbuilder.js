var xml2js = require('xml2js');
var XmlBuilder = (function () {
    function XmlBuilder(rootName) {
        this.builder = new xml2js.Builder({
            rootName: rootName,
            xmldec: {
                standalone: null,
                version: '1.0',
                encoding: 'UTF-8'
            },
            allowSurrogateChars: true
        });
    }
    XmlBuilder.prototype.build = function (rootObj) {
        return this.builder.buildObject(rootObj);
    };
    return XmlBuilder;
})();
exports.XmlBuilder = XmlBuilder;
