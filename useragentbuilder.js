var OS = require('os');
var fs = require('fs');
var path = require('path');
function version() {
    var v = process.version;
    return v[0] === 'v' ? v.substr(1) : v;
}
var UserAgentBuilder = (function () {
    function UserAgentBuilder() {
    }
    UserAgentBuilder.Build = function () {
        var pkg = { version: 'DEV' };
        var pkgconfig = path.resolve(__dirname, '..', '..', 'package.json');
        if (fs.existsSync(pkgconfig)) {
            pkg = JSON.parse(fs.readFileSync(pkgconfig, 'utf8'));
        }
        return 'esendex-node-sdk/' + pkg.version + '; node/' + version() + ' (' + OS.platform() + '; ' + process.arch + ')';
    };
    return UserAgentBuilder;
})();
exports.UserAgentBuilder = UserAgentBuilder;
