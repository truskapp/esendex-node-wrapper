var xml2js = require('xml2js');
var XmlParser = (function () {
    function XmlParser() {
        this.parser = new xml2js.Parser({ explicitArray: false, mergeAttrs: true });
    }
    XmlParser.prototype.parseString = function (str, cb) {
        this.parser.parseString(str, cb);
    };
    return XmlParser;
})();
exports.XmlParser = XmlParser;
