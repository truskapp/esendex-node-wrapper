var xmlparser = require('./xmlparser');
var xmlbuilder = require('./xmlbuilder');
var util = require('util');
var Conversation = (function () {
    function Conversation(client) {
        this.client = client;
        this.builder = new xmlbuilder.XmlBuilder('conversation');
        this.parser = new xmlparser.XmlParser();
    }
    Conversation.prototype.get = function (options, callback) {
        var _this = this;
        var path = '/v1.0/conversation';
        var phonenumber = options && options.phonenumber || null;
        if (phonenumber) {
            path = util.format('%s/%s/messages', path, phonenumber);
            delete options.phonenumber;
        }
        this.client.requesthandler.request('GET', path, options, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                callback(null, result);
            });
        });
    };
    return Conversation;
})();
exports.Conversation = Conversation;
