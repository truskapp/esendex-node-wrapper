var requesthandler = require('./requesthandler');
var conversation = require('./conversation');
var messages = require('./messages');
var accounts = require('./accounts');
var inbox = require('./inbox');
var batches = require('./batches');
var Client = (function () {
    function Client(options) {
        this.requesthandler = new requesthandler.RequestHandler(options);
        this.conversation = new conversation.Conversation(this);
        this.messages = new messages.Messages(this);
        this.accounts = new accounts.Accounts(this);
        this.inbox = new inbox.Inbox(this);
        this.batches = new batches.Batches(this);
    }
    return Client;
})();
exports.Client = Client;
