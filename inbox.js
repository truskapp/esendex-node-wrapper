var xmlparser = require('./xmlparser');
var util = require('util');
var Inbox = (function () {
    function Inbox(client) {
        this.client = client;
        this.parser = new xmlparser.XmlParser();
    }
    Inbox.prototype.get = function (options, callback) {
        var _this = this;
        var ref = options && options.accountreference || null;
        var path = ref ? util.format('/v1.0/inbox/%s/messages', ref) : '/v1.0/inbox/messages';
        this.client.requesthandler.request('GET', path, options, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                callback(null, result.messageheaders);
            });
        });
    };
    Inbox.prototype.update = function (options, callback) {
        var path = util.format('/v1.0/inbox/messages/%s', options.id);
        var query = { action: options.read ? 'read' : 'unread' };
        this.client.requesthandler.request('PUT', path, query, null, 200, callback);
    };
    Inbox.prototype.delete = function (id, callback) {
        var path = util.format('/v1.0/inbox/messages/%s', id);
        this.client.requesthandler.request('DELETE', path, null, null, 200, callback);
    };
    ;
    return Inbox;
})();
exports.Inbox = Inbox;
