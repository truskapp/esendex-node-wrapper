var xmlparser_1 = require('./xmlparser');
var xmlbuilder_1 = require('./xmlbuilder');
var util_1 = require('util');
var Batches = (function () {
    function Batches(client) {
        this.client = client;
        this.builder = new xmlbuilder_1.XmlBuilder('messagebatch');
        this.parser = new xmlparser_1.XmlParser();
    }
    Batches.prototype.get = function (options, callback) {
        var _this = this;
        this.client.requesthandler.request('GET', '/v1.0/messagebatches', options, null, 200, function (err, response) {
            if (err)
                return callback(err);
            _this.parser.parseString(response, function (err, result) {
                if (err)
                    return callback(err);
                callback(null, result.messagebatches);
            });
        });
    };
    ;
    Batches.prototype.cancel = function (batchId, callback) {
        var path = util_1.format('/v1.0/messagebatches/%s', batchId);
        this.client.requesthandler.request('DELETE', path, null, null, 204, callback);
    };
    ;
    Batches.prototype.rename = function (options, callback) {
        var path = util_1.format('/v1.1/messagebatches/%s', options.id);
        var requestObject = {
            '$': {
                xmlns: 'http://api.esendex.com/ns/'
            },
            name: options.name
        };
        var xml = this.builder.build(requestObject);
        this.client.requesthandler.request('PUT', path, null, xml, 204, callback);
    };
    ;
    return Batches;
})();
exports.Batches = Batches;
